import React from "react";
import "./style.css";
import logo from "./logo_200x45.png";

function Header(props) {
    return (
        <div>
            <img id="logo" alt="spokeo-logo" src={logo}/>
            <div id="content">{props.children}</div>
        </div>
    );
}

export default Header;
