import React from "react";
import "./style.css";

function LinkCard(props) {
  return (
    <li>
      <a
        href={`https://www.spokeo.com/search?q=${props.result}`}
        target="_blank"
      >
        {props.result}
      </a>
    </li>
  );
}

export default LinkCard;
