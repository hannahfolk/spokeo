/* global chrome */
import "./App.css";
import React, { Component } from "react";
import Wrapper from "./components/Wrapper";
import Header from "./components/Header";
import LinkCard from "./components/LinkCard";

class App extends Component {
  state = {
    results: [],
  };

  componentDidMount() {
    // Send a handshake over to the background
    chrome.extension.sendMessage({ data: "Handshake" }, (response) => {
      console.log(response);
    });
    // Once the background has received handshake, it will send over the results
    chrome.extension.onMessage.addListener((message, sender, sendResponse) => {
      // message.data are our results
      console.log(message.data);
      this.setState({
        results: message.data,
      });
    });
  }

  render() {
    return (
      <Wrapper>
        <Header>We found {this.state.results.length} results on Spokeo.</Header>
        <ol>
          {this.state.results.map((result) => (
            <LinkCard key={result} result={result} />
          ))}
        </ol>
      </Wrapper>
    );
  }
}

export default App;
