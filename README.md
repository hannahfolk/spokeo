# spokeo

## SPOKEO EXTENSION INITIAL URL SCREENSHOT:

1. ![Example 1](https://raw.githubusercontent.com/reburke286/Spokeo_ChromeExtension/master/docs/SpokeoExample1.JPG)
2. ![Example 2](https://raw.githubusercontent.com/reburke286/Spokeo_ChromeExtension/master/docs/SpokeoExample2.JPG)

## Overview and Description:

    This application was designed for Spokeo to assist its clients with a Chrome Extension platform. It combines Chrome's extension API, Spokeo's customer search engine along with a custom web scraper that uses Puppeteer to analyze the static or dynamic web content to find applicable phone numbers and emails to search. The front end is built with React.

## Installation

1. Fork the repo
2. CD into the folder
3. In your terminal type "npm install" to install all dependencies
4. Next type "npm start"
5. To build the webpack type "npm run-script build"
6. After webpack is deployed navigate to Chrome Browser
   a. In the Chrome Menu, navigate to More Tools and then into Extensions
   b. Turn on Developer Mode
7. Click on the button "Load unpacked" and choose the build folder from your repo

## Features and Usage:

<<<<<<< HEAD
This application is client-specific and was created for Spokeo to publish on Google Chrome pending completion.

    The extension performs a scrape of any dynamic or static web page and creates custom links of any available phone or email in the front end.

=======
This application is client-specific and was created for Spokeo to publish on Google Chrome pending completion.

The extension performs a scrape of any dynamic or static web page and creates custom links of any available phone or email in the front end.

> > > > > > > 45db5a9a8febfbe6a56302dda84c5209ae234c06

The extension uses local storage to save results of each web page for the client's convenience. It also alerts the client via extension badge to show the client how many results exist per page.

Currently... usage for this extension is client-specific, where Client will maintain and further develop. Also, READ ## Licensing below.

## Application Requirements:

Third Party APIs:

1. [Chrome Extension API](https://developer.chrome.com/extensions/api_index) created by Google to provides extensions with many special-purpose APIs, like messaging and setting icons.

<<<<<<< HEAD
Materialize (https://materializecss.com/) created and designed by Google, Material Design is a design language that combines the classic principles of successful design along with innovation and technology.

    Primary Node Packages:

=======

2. [Materialize](https://materializecss.com/) created and designed by Google, Material Design is a design language that combines the classic principles of successful design along with innovation and technology.
   > > > > > > > 45db5a9a8febfbe6a56302dda84c5209ae234c06

Primary Node Packages:

1. [Puppeteer](https://www.npmjs.com/package/puppeteer) is a Node library which provides a high-level API to control Chrome or Chromium over the DevTools Protocol. Puppeteer runs headless by default, but can be configured to run full (non-headless) Chrome or Chromium.

2. [Lodash](https://lodash.com/) makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc.

## Deployed to GitLab at: https://gitlab.com/hfolk25/spokeo

## Project Management and Collaborators:

Project Manager: Project Pitch and Repository Initialized by: Becca Burke (@reburke286)

Collaborators: Hannah Folk (@hannahfolk), Eduardo Urbaez (@eurbaezjr), and Marie Gilbert (@marieegilbert)

## Resources:

<<<<<<< HEAD
gitlab: https://gitlab.com/

    Google Fonts: https://fonts.google.com/

=======
gitlab: https://gitlab.com/

Google Fonts: https://fonts.google.com/

> > > > > > > 45db5a9a8febfbe6a56302dda84c5209ae234c06

MDN web docs moz://a: https://developer.mozilla.org/en-US/

stack overflow: https://stackoverflow.com/

<<<<<<< HEAD
trello: https://trello.com/

    Also SEE Third Party APIs listed under Application Requirements.

=======
trello: https://trello.com/

Also SEE Third Party APIs listed under Application Requirements.

> > > > > > > 45db5a9a8febfbe6a56302dda84c5209ae234c06

## Credits:

2019 Trilogy Education Services: https://www.trilogyed.com/ - Educational Instruction

UCLA Extension Full Stack Web Development Bootcamp: https://bootcamp.uclaextension.edu/coding/ Educational Instruction and Facility Usages

Omar Patel - UCLA Extension Lead Intructor and Student Support

<<<<<<< HEAD
Julio Valdez, Peter Park - UCLA Extension TAs and Student Support
=======
Julio Valdez, Peter Park - UCLA Extension TAs and Student Support

## Licensing:

PERMISSION NOT GRANTED FOR USE OF THIS APPLICATION.

THIS APPLICATION IS SPECIFIC TO CLIENT USE ONLY.

> > > > > > > 45db5a9a8febfbe6a56302dda84c5209ae234c06

## Licensing:

    PERMISSION NOT GRANTED FOR USE OF THIS APPLICATION.

    THIS APPLICATION IS SPECIFIC TO CLIENT USE ONLY.
